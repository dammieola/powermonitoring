from django.shortcuts import render

# Create your views here.

def meters(request):
    return render(request, 'partials/meters.html')

def add_meter(request):
    return render(request, 'partials/add_meter.html')

def edit_meter(request):
    return render(request, 'partials/edit_meter.html')

def transformers(request):
    return render(request, 'partials/transformers.html')

def add_transformers(request):
    return render(request, 'partials/add_transformers.html')

def edit_transformers(request):
    return render(request, 'partials/edit_transformers.html')

def users(request):
    return render(request, 'partials/user.html')

def add_user(request):
    return render(request, 'partials/add_user.html')

def edit_user(request):
    return render(request, 'partials/edit_user.html')
def dashboard(request):
    return render(request, 'partials/dashboard.html')
